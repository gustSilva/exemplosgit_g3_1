package Exercicios30out;

public class Ponto {

	private int x;
	private int y;

	public Ponto() {

	}

	public Ponto(int x, int y) {
		this.x = x;
		this.y = y;

	}

	// getX permite que o valor de x seja utilizado por outros métodos fora desta
	// classe sem ser alterado
	public int getX() {
		return this.x;

	}

	public int getY() {
		return this.y;

	}

	@Override
	public String toString() {
		return "(" + this.x + "," + this.y + ")";
	}

	@Override
	public boolean equals(Object other) {
		boolean result = false;
		if (other instanceof Ponto) {
			Ponto ponto2 = (Ponto) other;
			if (ponto2.x == this.x && ponto2.y == this.y) {
				result = true;
			}
		}
		return result;
	}

}
