package exercicios31102017;

import java.util.ArrayList;
import java.util.List;

public class Matrix {

	int[][] matrix;
	int nRows, nColumns;

	/**
	 * Constructor for the Matrix class with nRows and nColumns as parameters.
	 * @param nRows
	 * @param nColumns
	 */
	public Matrix(int nRows, int nColumns) {
		this.nRows = nRows;
		this.nColumns = nColumns;
		matrix = new int[this.nRows][this.nColumns];
	}

	/**
	 * Constructor for the Matrix class with a 2D Array as parameter.
	 * @param matrixArray
	 */
	public Matrix(int[][] matrixArray) {
		
		if(parameterIsValid(matrixArray)) {
			this.nRows = matrixArray.length;
			this.nColumns = matrixArray[0].length;
			this.matrix = new int[this.nRows][this.nColumns];
			
			for (int i = 0; i < this.nRows; i++) {
				for (int j = 0; j < this.nColumns; j++) {
					this.matrix[i][j] = matrixArray[i][j];
				}
			}
		} else {
			throw new IllegalArgumentException();
		}
	}

	public boolean parameterIsValid(int[][] matrixArray) {
		int rowLength = matrixArray[0].length;
		boolean result = true;

		for(int i = 0; (i < matrixArray.length) && (result); i++) {
			result = (matrixArray[i].length == rowLength);
		}
		
		return result;
	}

	/**
	 * Returns the sum of the Matrix instance with another Matrix instance.
	 * @param b
	 * @return
	 */
	public Matrix sumWith(Matrix b) {
		Matrix result = new Matrix(b.nRows, b.nColumns);

		for(int i = 0; i < this.nRows; i++) {
			for(int j = 0; j < this.nColumns; j++) {
				result.matrix[i][j] = (this.matrix[i][j] + b.matrix[i][j]);
			}
		}

		return result;
	}

	/**
	 * Getter for the matrix instance variable.
	 * @return
	 */
	public int[][] getMatrix() {

		return this.matrix;
	}
	
	public int getNRows() {
		
		return this.nRows;
	}
	
	public int getNColumns() {
		
		return this.nColumns;
	}


	/**
	 * Returns an array list with all the positions found in the matrix instance variable that
	 * corresponded to the number 'number' passed as parameter.
	 * @param number
	 * @return
	 */
	public List<Ponto> getPontosWithNumbers(int number) {
		List<Ponto> result = new ArrayList<Ponto>();

		for(int i = 0; i < this.nRows; i++) {
			for(int j = 0; j < this.nColumns; j++) {
				if(this.matrix[i][j] == number) {
					result.add(new Ponto(i,j));
				}
			}
		}

		return result;
	}

	/**
	 * Sets the position P in the matrix instance variable to the value 'valor'
	 * @param p
	 * @param valor
	 */
	public void setValor(Ponto p, int valor) {
		if((p.getX() >= this.nRows && p.getX() >= 0) || (p.getY() >= this.nColumns && p.getY() >= 0)) {
			return;
		} else {
			this.matrix[p.getX()][p.getY()] = valor;
		}
	}

	/**
	 * Getter for the value in the matrix instance variable that corresponds to the
	 * coordinates of p.
	 * @param p
	 * @return
	 */
	public int getValor(Ponto p) {
		return this.matrix[p.getX()][p.getY()];
	}

	@Override
	public boolean equals(Object other) {
		boolean result = false;
		if(other instanceof Matrix) {
			Matrix m2 = (Matrix) other;
			if((m2.nRows == this.nRows) &&(m2.nColumns == this.nColumns)) {
				result = true;
				for(int i = 0; i < this.nRows && result; i++) {
					for(int j = 0; j < this.nColumns && result; j++) {
						result = (m2.matrix[i][j] == this.matrix[i][j]);
					}
				}
			}
		}

		return result;
	}

	@Override
	public String toString() {
		String result = "";
		if(this.nRows > 0 && this.nColumns > 0) {
			for(int i = 0; i < this.nRows; i++) {
				for(int j = 0; j < this.nColumns; j++) {
					result += (this.matrix[i][j] + " ");
				}
				result+= "\n";
			}
		}

		return result;
	}

	/**
	 * Returns the sum of corners and the center of the matrix.
	 * If the matrix has an even number of columns or rows, returns zero.
	 * @return
	 */
	public int sumOfCornersAndCenterOfMatrix() {

		int sum = 0;
		int centerRow;
		int centerColumn;

		if((this.nColumns % 2 == 0) || (this.nRows % 2 == 0) || isOneByOneMatrix()) {
			return sum;
		} else {
			centerRow = this.nRows / 2;
			centerColumn = this.nColumns / 2;
			sum = this.matrix[0][0] + this.matrix[0][this.nColumns - 1] +
					this.matrix[this.nRows - 1][0] + this.matrix[this.nRows - 1][this.nColumns - 1] +
					this.matrix[centerRow][centerColumn];
			return sum;
		}

	}

	/**
	 * Checks if the matrix instance variable is a one by one matrix
	 * @return
	 */
	public boolean isOneByOneMatrix() {

		return (this.nColumns == 1 && this.nRows == 1);
	}

	/**
	 * Returns a new Matrix object that is the transposition of the Matrix instance.
	 * @return
	 */
	public Matrix transposeMatrix() {
		Matrix transposed = new Matrix(this.nColumns, this.nRows);

		for(int i = 0; i < transposed.nRows; i++) {
			for(int j = 0; j < transposed.nColumns; j++) {
				transposed.matrix[i][j] = this.matrix[j][i];
			}
		}

		return transposed;
	}

	/**
	 * Checks is matrix is quadratic
	 * @param matrix
	 * @return
	 */
	public boolean isMatrixQuadratic() {

		return (this.nRows == this.nColumns);
	}

	/**
	 * Checks if matrix is upper triangular
	 * @param matrix
	 * @return a boolean
	 */
	public boolean isMatrixTriangularUpper() {

		if (!isMatrixQuadratic()) { return false; }

		int aux = 1;
		boolean result = true;

		for (int i = 0; i < this.nRows; i++) {
			for (int j = aux; (j < this.nColumns) && (result == true); j++) {
				result = (this.matrix[i][j] == 0);
			}
			aux++;
		}

		return result;
	}

	/**
	 * Checks if matrix is lower triangular
	 * @param matrix
	 * @return a boolean
	 */
	public boolean isMatrixTriangularLower() {

		if (!isMatrixQuadratic()) { return false; }

		int aux = 1;
		boolean result = true;

		for (int i = aux; i < this.nRows; i++) {
			for (int j = 0; (j < aux) && (result == true); j++) {
				result = (matrix[i][j] == 0);
			}
			aux++;
		}

		return result;
	}


	/**
	 * Checks is matrix is triangular
	 * @param matrix
	 * @return
	 */
	public boolean isMatrixTriangular () {

		return (isMatrixQuadratic() && (isMatrixTriangularLower() || 
				isMatrixTriangularUpper()));

	}

	/**
	 * Checks if matrix is symmetrical
	 * @param matrix
	 * @return
	 */
	public boolean isMatrixSymmetrical() {

		if(!isMatrixQuadratic()) { return false; }

		boolean result = true;

		for (int i = 0; i < this.nRows; i++) {
			for (int j = 0; (j < this.nColumns) && (result); j++) {
				result = (this.matrix[i][j] == this.matrix[j][i]);
			}
		}

		return result;
	}
	
	@Override
	public Matrix clone() {
		return new Matrix(getMatrix());
		
	}
}
