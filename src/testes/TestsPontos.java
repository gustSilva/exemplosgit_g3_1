package Tests;

import getPonto.Ponto;
import getPonto.Utils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestsPontos {

	@Test
	public void canItFindTheLetter() {
		char[][] m = {
				{'a', 'b', 'c'},
				{'a', 'b', 'c'},
				{'a', 'b', 'c'},
		};
		List<Ponto> expectedResult = new ArrayList<Ponto>(Arrays.asList(
				new Ponto(0, 2),
				new Ponto(1, 2),
				new Ponto(2, 2)));
		List<Ponto> result = Utils.getPonto(m, 'c');
		assertEquals(expectedResult, result);
	}

}
